<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TEMPLATENAME
 */
	$fav = esc_attr( get_option( 'favicon_url' ) );
	$logo = esc_attr( get_option( 'logo_url' ) );
	$fb = esc_attr( get_option('fb_url') );
	$twit = esc_attr( get_option('twitter_url') );
	$insta = esc_attr( get_option('insta_url') );
	$addressUrl = nl2br(esc_attr( get_option( 'address_url' ) ));
	$phoneUrl = esc_attr( get_option( 'phone_url' ) );
	$copyright = esc_attr( get_option( 'copyright_url' ) );
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
		<?php
		bloginfo('name');
		echo ' | ';
        if (wp_title('', false)) {
			echo "";
        } else {
            echo bloginfo('description');
        } wp_title('');
		?>
	</title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if( is_singular() && pings_open( get_queried_object() ) ): ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	
    <script src="<?php echo get_template_directory_uri()?>/assets/js/jquery.min.js"></script>
	<?php wp_head(); ?>
	<?php 
		$custom_css = get_option( 'theme_css' );
		if(!empty($custom_css)) {
			?>
				<?php echo '<style type="text/css">'. $custom_css. '</style> '; ?>
			<?php
		}
	?>
</head>
<body <?php body_class(); ?>>

<!-- header -->
<header class="h-menu-header">
	<div class="h-top-head">
		<h1>遺品整理・特殊清掃・相続不動産売却査定は、プロにお任せ！</h1>
	</div>
	<div class="h-nav-menu">
		<h3 class="h-logo">
			<a href="<?php bloginfo('home'); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/h-logo.svg" class="h-pc-logo" alt="H Life Logo">
			</a>
		</h3>
		<ul class="h-social-phone">
			<li>
				<a href="tel:0120-777-636" class="h-kakao">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/kakaoimg01.png" alt="">
				</a>
			</li>
			<li>
				<a href="https://lin.ee/5mdbiYD" class="h-line" target="__blank">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/line.png" alt="Line" class="v-pc">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/linesp.svg" alt="Line" class="v-sp">
				</a>
			</li>
			<li class="scroll-div">
				<a href="#h-contact" class="h-line">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/line02.png" alt="Quote" class="v-pc">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/linesp02.svg" alt="Line" class="v-sp">
				</a>
			</li>
			<li class="h-show-sp">
				<a href="tel:0120-777-636" class="h-kakao">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/kakaoimg01.png" alt="">
				</a>
			</li>
		</ul>
	</div>
</header>
<!-- end of header -->

<main>
<?php
	if( is_front_page() ){
		?>

		<!-- main visual -->
		<section class="h-main-visual">
			<div class="cntr">
				<div class="h-main-people-cleaning">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/people/peoplecleaning.png" alt="People Cleaning">
				</div>
				<div class="h-main-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/main/main01.png" class="h-main01 animateMe" data-animation="zoomIn" alt="Quote">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/main/main03.png" class="h-main02 animateMe" data-animation="fadeInDown" alt="Quote">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/main/main03sp.png" class="h-main02sp" alt="Quote">
				</div>
				<div class="h-main-img h-main-img02">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/main/main02.png" class="h-main03 animateMe" data-animation="zoomIn" alt="Quote">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/main/main04.png" class="h-main04 animateMe" data-animation="fadeInDown" alt="Quote">
				</div>
				<ul class="main-text-list-new">
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/main/maintextnew01-1.png" class="main-pc" data-animation="fadeInLeft" alt="Quote">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/main/maintextnew01-1-sp.png" class="main-sp" data-animation="fadeInLeft" alt="Quote">
					</li>
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/main/maintextnew02-2.png" class="main-pc" data-animation="fadeInRight" alt="Quote">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/main/maintextnew02-2-sp.png" class="main-sp" data-animation="fadeInRight" alt="Quote">
					</li>
				</ul>
			</div>
		</section>
		<!-- end of main visual -->

		<!-- clean list -->
		<section class="h-clean-list-wrp">
			<div class="cntr">
				<ul class="h-clean-lists">
					<li class="animateMe" data-animation="fadeInUp">
						<div class="h-clean-round">
							<div class="h-clean-img">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/list/icon01.png" alt="Quote">
								<p>遺品整理</p>
							</div>
						</div>
					</li>
					<li class="animateMe" data-animation="fadeInUp" data-animation-delay="0.5s">
						<div class="h-clean-round">
							<div class="h-clean-img">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/list/icon02.png" alt="Quote">
								<p>貴重品探索</p>
							</div>
						</div>
					</li>
					<li class="animateMe" data-animation="fadeInUp" data-animation-delay="1s">
						<div class="h-clean-round">
							<div class="h-clean-img">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/list/icon03.png" alt="Quote">
								<p>特殊清掃</p>
							</div>
						</div>
					</li>
					<li class="animateMe" data-animation="fadeInUp" data-animation-delay="1.5s">
						<div class="h-clean-round">
							<div class="h-clean-img">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/list/icon04.png" alt="Quote">
								<p>
									不動産売却<br>
									査定
								</p>
							</div>
						</div>
					</li>
					<li class="animateMe" data-animation="fadeInUp" data-animation-delay="2s">
						<div class="h-clean-round">
							<div class="h-clean-img">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/list/icon05.png" alt="Quote">
								<p>遺品供養</p>
							</div>
						</div>
					</li>
					<li class="animateMe" data-animation="fadeInUp" data-animation-delay="2.5s">
						<div class="h-clean-round">
							<div class="h-clean-img">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/list/icon06.png" alt="Quote">
								<p>生前整理</p>
							</div>
						</div>
					</li>
					<li class="animateMe" data-animation="fadeInUp" data-animation-delay="3s">
						<div class="h-clean-round">
							<div class="h-clean-img">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/list/icon07.png" alt="Quote">
								<p>
									遺品分割協議書<br>
									の作成
								</p>
							</div>
						</div>
					</li>
					<li class="animateMe" data-animation="fadeInUp" data-animation-delay="3.5s">
						<div class="h-clean-round">
							<div class="h-clean-img">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/list/icon08.png" alt="Quote">
								<p>相続登記</p>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</section>
		<!-- end of clean list -->

		<?php
	}
?>