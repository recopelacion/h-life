<?php
/**
 * The template for displaying all pages
 * Template Name: Homepage
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<!-- banner -->
<?php get_template_part( 'template-parts/template-banner'); ?>
<!-- end of banner -->

<!-- price list -->
<section class="h-price-list">
    <div class="cntr">
        <div class="h-title">
            <h2 class="animateMe" data-animation="fadeInDown">料金表</h2>
            <p class="animateMe" data-animation="fadeInDown">
                業界最安値に挑戦し、お客様に、よりご利用いただきやすいサービスを目指しております。<br>
                是非他社様のサービスと比べてみてください！
            </p>
        </div>
        <div class="h-table-list">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/table01.png" alt="Price List" class="v-pc">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/table02.png" alt="Price List" class="v-sp">
        </div>
        <p class="h-price-tag">※場所、お部屋の状態、正確な面積、規模などにより変動する場合がございます。</p>
    </div>
</section>
<!-- end of price list -->

<!-- flow inquiry -->
<section class="h-flow-inquiry">
    <div class="cntr">
        <div class="h-title h-title-def">
            <h2 class="animateMe" data-animation="fadeInDown">お問い合わせからの流れ</h2>
            <p class="animateMe" data-animation="fadeInDown">
                お問い合わせから、遺品整理・特殊清掃、完了までの流れをご紹介。
            </p>
        </div>
        <div class="gap gap-10 gap-0-xs">
            <div class="md-6 xs-12">
                <a href="#" class="h-card">
                    <span class="h-card-count">1</span>
                    <div class="h-card-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/card/card01.jpg" alt="Price List">
                    </div>
                    <div class="h-card-cont">
                        <h4 class="h-card-title">お問い合わせ</h4>
                        <p class="h-card-desc">年中無休!お気軽にお電話、メール、LINE にてお問い合わせ下さい。(受付時間 9:30 ~ 22:00)</p>
                    </div>
                </a>
            </div>
            <div class="md-6 xs-12">
                <a href="#" class="h-card">
                    <span class="h-card-count h-card-count2">2</span>
                    <div class="h-card-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/card/card02.jpg" alt="Price List">
                    </div>
                    <div class="h-card-cont">
                        <h4 class="h-card-title">お見積り</h4>
                        <p class="h-card-desc">現地にて 10 ~ 20 分程度のお時間を頂きます。その際にご希望、ご要望が ございましたらお気軽にお申しつけください。</p>
                    </div>
                </a>
            </div>
            <div class="md-6 xs-12">
                <a href="#" class="h-card">
                    <span class="h-card-count h-card-count3">3</span>
                    <div class="h-card-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/card/card03.jpg" alt="Price List">
                    </div>
                    <div class="h-card-cont">
                        <h4 class="h-card-title">現地作業</h4>
                        <p class="h-card-desc">ご希望の日時で作業をさせて頂きます。貴重品の探索や大切なものの仕分けなど お気軽にお申しつけください。</p>
                    </div>
                </a>
            </div>
            <div class="md-6 xs-12">
                <a href="#" class="h-card">
                    <span class="h-card-count h-card-count4">4</span>
                    <div class="h-card-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/card/card04.jpg" alt="Price List">
                    </div>
                    <div class="h-card-cont">
                        <h4 class="h-card-title">お支払い</h4>
                        <p class="h-card-desc">作業完了後の立会い確認をして頂きます。遠方でお越し頂くことが難しい場合は、写真をメールさせて頂き、チェックして頂くことも可能です。</p>
                    </div>
                </a>
            </div>
        </div>
        <p class="h-tagline-desc">こんなご相談、お任せ下さい！！</p>
        <div class="h-title-sec">
            <h3>
                すべてワンストップで<br class="v-sp">お手伝いさせて頂きます。<br>
                担当の遺品整理士、<br class="v-sp">相続診断士にお任せください！！
            </h3>
        </div>
        <ul class="h-card-wrp-people">
            <li>
                <div class="h-card-people">
                    <div class="h-card-people-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/people/people01.png" alt="People">
                    </div>
                    <h4 class="h-card-people-text">弁護士</h4>
                </div>
            </li>
            <li>
                <div class="h-card-people">
                    <div class="h-card-people-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/people/people02.png" alt="People">
                    </div>
                    <h4 class="h-card-people-text">司法書士</h4>
                </div>
            </li>
            <li>
                <div class="h-card-people">
                    <div class="h-card-people-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/people/people03.png" alt="People">
                    </div>
                    <h4 class="h-card-people-text">宅地建物取引士</h4>
                </div>
            </li>
            <li>
                <div class="h-card-people">
                    <div class="h-card-people-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/people/people04.png" alt="People">
                    </div>
                    <h4 class="h-card-people-text">税理士</h4>
                </div>
            </li>
            <li>
                <div class="h-card-people">
                    <div class="h-card-people-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/people/people05.png" alt="People">
                    </div>
                    <h4 class="h-card-people-text">土地家屋調査士</h4>
                </div>
            </li>
        </ul>
        <div class="h-title-sec h-title-sec-sm">
            <h3>
                私たちは、孤独死や事故死による<br class="v-sp">遺品整理、特殊清掃、相続のプロです。<br>
                気になることなどがあればお気軽に<br class="v-sp">ご相談ください。
            </h3>
        </div>
    </div>
</section>
<!-- end of flow inquiry -->

<!-- banner -->
<?php get_template_part( 'template-parts/template-banner'); ?>
<!-- end of banner -->

<!-- trouble -->
<section class="h-trouble">
    <div class="cntr">
        <div class="h-title">
            <h2 class="animateMe" data-animation="fadeInDown">
                みなさん<br class="v-sp">このような事で<br>
                お困りです
            </h2>
            <p class="animateMe" data-animation="fadeInDown">
                みなさんのお困りごと、<br class="v-sp">一度私たちにご相談ください。<br>
                お悩みに合わせて、<br class="v-sp">プロフェッショナルが解決へ導きます。
            </p>
        </div>
        <ul class="h-trouble-list">
            <li>
                <div class="h-trouble-list-item">
                    <h4><span>通帳</span>や<span>権利書</span>など大切なものを探してほしい。</h4>
                </div>
            </li>
            <li>
                <div class="h-trouble-list-item">
                    <h4>遠方に住んでいて<span>何度も行き来ができない</span>。</h4>
                </div>
            </li>
            <li>
                <div class="h-trouble-list-item">
                    <h4>孤独死の発見が遅れ近隣から<span>クレーム</span>がでている。</h4>
                </div>
            </li>
            <li>
                <div class="h-trouble-list-item">
                    <h4><span>不動産を相続</span>したがどうしていいかわからない。</h4>
                </div>
            </li>
            <li>
                <div class="h-trouble-list-item">
                    <h4>多額の<span>費用をかける余裕がない</span>。</h4>
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- end of trouble -->

<!-- group people -->
<section class="h-group-people">
    <div class="cntr">
        <div class="h-group-people-text">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/people/grouptext.png" alt="Line" class="v-pc h-grouptext">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/people/grouptext02.png" alt="Line" class="v-sp h-grouptext">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/people/grouppeople.png" alt="Line" class="h-grouppeople">
        </div>
    </div>
</section>
<!-- end of group people -->

<!-- contact us -->
<section id="h-contact" class="h-contact-us">
    <div class="cntr">
        <div class="h-title h-title-def">
            <h2 class="animateMe" data-animation="fadeInDown">お問い合わせ</h2>
            <p class="animateMe" data-animation="fadeInDown">
                お見積りやサービスのご利用について、<br class="v-sp">下記のフォームよりお問い合わせください。<br>
                お電話からのご相談も<br class="v-sp">承っておりますのでご連絡ください。
            </p>
        </div>
        <div class="h-card-banner h-card-bannerv2">
            <div class="h-card-banner-tit">
                <h4>必ず相見積もりして下さい！<br class="v-sp">その一社に弊社を入れてください！</h4>
            </div>
            <ul class="h-banner-social">
                <li>
                    <div class="h-card-banner-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/h-person01.png" alt="Title">
                    </div>
                </li>
                <li>
                    <div class="h-card-banner-wrp">
                        <h4 class="h-card-banner-tit">お気軽にお電話ください！</h4>
                        <a href="tel:0120-777-636" class="h-kakao">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/kakaoimg02.png" alt="">
                        </a>
                    </div>
                </li>
            </ul>
        </div>
        <?php echo do_shortcode('[mwform_formkey key="30"]'); ?>
    </div>
</section>
<!-- end of contact us -->

<!-- blog -->
<section class="h-blog">
    <div class="cntr">
        <div class="h-title h-title-def animateMe" data-animation="fadeInDown">
            <h2>ブログ</h2>
        </div>
        
        <?php
        // news query
        $news_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>7)); ?>
        <?php if ( $news_query->have_posts() ) : ?>
            <ul class="h-blog-list">
                <!-- the loop -->
                <?php while ( $news_query->have_posts() ) : $news_query->the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>" class="h-blog-list-item">
                            <div class="h-blog-date-cat">
                                <span class="h-blog-date">
                                    <?php the_time('Y m.d'); ?>
                                </span>
                                <?php
                                    $terms = get_the_terms( $post->ID , 'category' );
                                    if(is_array($terms) || is_object($terms)){
                                        foreach ( $terms as $term ) {
                                            ?>
                                            <span class="h-blog-cat <?php echo $term->slug; ?>">
                                                <?php echo $term->name; ?>
                                            </span>
                                        <?php
                                        }
                                    }
                                ?>
                            </div>
                            <h4 class="h-blog-title"><?php the_title(); ?></h4>
                        </a> 
                    </li>
                <?php endwhile; ?>
                <!-- end of the loop -->
            </ul>
            <div class="h-btn h-btn-green">
                <a href="<?php bloginfo('url'); ?>/blog">一覧を見る</a>
            </div>
            <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <h4 class="h-no-post"><?php _e( 'Sorry, no blogs matched your criteria.' ); ?></h4>
        <?php endif; ?>
    </div>
</section>
<!-- end of blog -->



<?php
get_footer();