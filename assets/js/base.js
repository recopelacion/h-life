/*------------------------------
Mobile Menu (toggleClass to activate transitions)
------------------------------*/
(function($){

    const navbar = $('.navbar-toggler')

    navbar.on("click",function(e){
        e.preventDefault()
        if( $(".navbar-type").hasClass("is-toggled")) {
            $(this).removeClass("is-toggled")
            $(".navbar-type").removeClass("is-toggled")
        }else{
            $(this).addClass("is-toggled")
            $(".navbar-type").addClass("is-toggled")
        }
       
        return false
    })

    let navbarH = $('.navbar').innerHeight();
    let w = $(window).width();

    if( w < 769 ){
        $('.navbar-collapse').css({
            'margin-top': navbarH
        })
    } 

    if( w < 425 ){
        var scrollLink = $('.scroll-div a');
        scrollLink.on('click', function() {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.substr(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 80
                }, 1000);
                return false;
            }
        });
    }else{
        var scrollLink = $('.scroll-div a');
        scrollLink.on('click', function() {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.substr(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 130
                }, 1000);
                return false;
            }
        });
    }

    $(window).resize(function(){
        if( w < 769 ){
            $('.navbar-collapse').css({
                'margin-top': navbarH
            })
        }
    })

    
    
    

})(jQuery);