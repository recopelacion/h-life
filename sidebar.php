<?php
/**
 * The template for displaying sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */
?>

<!-- popular posts -->
<div class="h-sidebar-post">
    <h4 class="h-side-tit">人気記事</h4>
    <ul class="h-popular-posts">
        

        <?php 
            $popularpost = new WP_Query( array( 'posts_per_page' => 6, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
            while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>

            <li>
                <a href="<?php the_permalink(); ?>" class="h-popular-item">
                    <div class="h-popular-img">
                        <?php if(has_post_thumbnail()): ?>
                            <?php
                                $thumb_id = get_post_thumbnail_id(get_the_ID());
                                $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                            ?>
                            <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                        <?php else: ?>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/thumb.jpg" alt="" class="is-wide">
                        <?php endif; ?>
                    </div>
                    <h3 class="h-popular-title"><?php echo mb_strimwidth(get_the_title(), 0, 30, '...'); ?></h3>
                </a>
            </li>
            
            <?php endwhile;
        ?>

    </ul>
</div>
<!-- end of popular posts -->

<!-- category posts -->
<div class="h-sidebar-post h-sidebar-post-cat">
    <h4 class="h-side-tit">カテゴリ</h4>
    <?php
        $cat_args = array('orderby' => 'name', 'show_count' => '1', 'hierarchical' => '0','taxonomy' => 'category');?>
    <ul class="h-popular-posts">
        <!-- <li>
            <a href="" class="h-popular-item">
                <h3 class="h-popular-title">カテゴリ名が入ります。</h3>
            </a>
        </li>
        <li>
            <a href="" class="h-popular-item">
                <h3 class="h-popular-title">カテゴリ名が入ります。</h3>
            </a>
        </li> -->
        <?php
            $cat_args['title_li'] = '';
            wp_list_categories(apply_filters('', $cat_args));
        ?>
    </ul>
</div>
<!-- end of category posts -->

<!-- archive posts -->
<div class="h-sidebar-post h-sidebar-postv2">
    <h4 class="h-side-tit">アーカイブ</h4>
    <ul class="h-popular-posts h-popular-posts-year">
        <?php 
                $query = $wpdb->prepare('
                SELECT YEAR(%1$s.post_date) AS `year`, count(%1$s.ID) as `posts`
                FROM %1$s
                WHERE %1$s.post_type IN ("post")
                AND %1$s.post_status IN ("publish")
                GROUP BY YEAR(%1$s.post_date)
                ORDER BY %1$s.post_date DESC',
                $wpdb->posts
            );
            $results = $wpdb->get_results($query);
            /** Create the '$years' array, a list of all yearly archives */
            $years = array();
            if(!empty($results)) : foreach($results as $result) :

                $count = '<span class="lower">(' . $result->posts . ')</span>';      // The number of posts in the current year
                $url = get_year_link($result->year);                                    // The archive link URL for the current year
                $text = '<li class="year-list">' . $result->year . ' ' . $count . '</li>';                // The archive link text for the current year
                $years[] = get_archives_link($url, $text, 'html');                      // Create the archive link for the current year

            endforeach;
            endif;
            /** Output the custom archive list */
            echo join("\n", $years);
            ?>
    </ul>
</div>
<!-- end of archive posts -->