<?php
/**
 * The template for displaying news pages
 *
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<!-- tagline -->
<section class="h-tagline-title">
    <div class="cntr">
        <h4><?php the_title(); ?></h4>
    </div>
</section>
<!-- tagline -->


<section class="h-page-news h-page-news-details">
    <div class="cntr">
        <!-- breadcrumbs -->
        <ul class="h-breadcrumbs">
            <li>
                <a href="<?php bloginfo('home'); ?>">Top</a>
            </li>
            <li>
                <a href="<?php bloginfo('url'); ?>/blog">ブログ一覧</a>
            </li>
            <li>
                <span><?php the_title(); ?></span>
            </li>
        </ul>
        <!-- end of breadcrumbs -->
        <div class="gap gap-10 gap-0-xs">
            <div class="md-8 xs-12">
                <div class="h-page-news-det-wrp">
                    <span class="h-page-news-det-date"><?php the_date('Y.m.d'); ?></span>
                    <?php
                        $terms = get_the_terms( $post->ID , 'category' );
                        if(is_array($terms) || is_object($terms)){
                            foreach ( $terms as $term ) {
                                ?>
                                <span class="h-page-news-det-cat <?php echo $term->slug; ?>">
                                    <?php echo $term->name; ?>
                                </span>
                            <?php
                            }
                        }
                    ?>
                    <div class="h-page-news-det-img">
                        <?php if(has_post_thumbnail()): ?>
                            <?php
                                $thumb_id = get_post_thumbnail_id(get_the_ID());
                                $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                            ?>
                            <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                        <?php else: ?>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/thumb.jpg" alt="" class="is-wide">
                        <?php endif; ?>
                    </div>
                    <h3 class="h-page-news-det-title"><?php the_title(); ?></h3>
                    <div class="h-page-news-cont">
                        <?php the_content(); ?>
                    </div>
                    <div class="h-page-news-tags">
                        <h4>タグ</h4>
                        <?php
                        if ( is_singular() ) :
                            echo get_the_tag_list(
                                '<ul class="h-tag-list"><li>',
                                '</li><li>',
                                '</li></ul>',
                                get_queried_object_id()
                            );
                        endif;
                        ?>
                    </div>
                    <div class="h-page-news-pager">
                        <?php
                            //Get The Next Post Thumbnail and link
                            $nextPost = get_next_post();
                            $nextThumbnail = get_the_post_thumbnail( $nextPost->ID );
                            echo '<div class="h-pager-name-prev">';
                            echo '<h3>前の投稿</h3>';
                            next_post_link( '%link', "$nextThumbnail <h4>%title</h4>" );
                            echo '</div>';

                            //Get Previous Post Thumbnail and link
                            $prevPost = get_previous_post();
                            $prevThumbnail = get_the_post_thumbnail( $prevPost->ID );
                            echo '<div class="h-pager-name-next">';
                            echo '<h3>次の投稿</h3>';
                            previous_post_link( '%link', "$prevThumbnail <h4>%title</h4>" );
                            echo '</div>';
                        ?>
                    </div>
                </div>
            </div>
            <div class="md-4 xs-12">
                <?php echo get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();