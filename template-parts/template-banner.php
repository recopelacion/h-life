<section class="h-banner">
    <div class="cntr">
        <div class="h-slant-title">
            <p class="animateMe" data-animation="zoomIn"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/title/h-slant-tit01.png" alt="Title"></p>
            <h2 class="animateMe" data-animation="zoomIn">全国対応可能！業界最安値！</h2>
        </div>
        <ul class="h-person">
            <li>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/h-person01.png" alt="Title">
            </li>
            <li class="animateMe" data-animation="fadeInUp">
                <div class="h-box">
                    <span class="h-box-l">
                        出張・<br>
                        見積もり
                    </span>
                    <span class="h-box-r">
                        0<small>円</small>
                    </span>
                </div>
            </li>
            <li class="animateMe" data-animation="fadeInUp" data-animation-delay="0.5s">
                <div class="h-box">
                    <span class="h-box-l">
                        権利品<br>
                        貴重品探索
                    </span>
                    <span class="h-box-r">
                        0<small>円</small>
                    </span>
                </div>
            </li>
            <li class="animateMe" data-animation="fadeInUp" data-animation-delay="1s">
                <div class="h-box">
                    <span class="h-box-l">
                        遺品<br>
                        供養
                    </span>
                    <span class="h-box-r">
                        0<small>円</small>
                    </span>
                </div>
            </li>
        </ul>
        <div class="h-card-banner">
            <div class="h-card-banner-tit">
                <h4>必ず相見積もりして下さい！その一社に弊社を入れてください！</h4>
            </div>
            <ul class="h-banner-social">
                <li class="animateMe" data-animation="fadeInDown">
                    <div class="h-card-banner-wrp">
                        <h4 class="h-card-banner-tit">お気軽にお電話ください！</h4>
                        <a href="tel:0120-777-636" class="h-kakao">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/kakaoimg02.png" alt="">
                        </a>
                    </div>
                </li>
                <li class="animateMe" data-animation="fadeInDown">
                    <div class="h-card-banner-wrp">
                        <h4 class="h-card-banner-tit">24時間いつでもご連絡いただけます！</h4>
                        <div class="h-banner-talk scroll-div">
                            <a href="https://lin.ee/5mdbiYD" class="h-line" target="__blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/line03.png" alt="Line">
                            </a>
                            <a href="#h-contact" class="h-line">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/line04.png" alt="Line">
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>