<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TEMPLATENAME
 */

?>
	</main>
	<footer class="h-footer">
		<div class="cntr">
			<p><a href="http://house-produce.com/" target="__blank">運営会社</a> | © 2020 ハートライフ </p>
		</div>
	</footer>
	
	<?php wp_footer(); ?>
	<?php 
		$custom_js = get_option( 'theme_js' );
		if(!empty($custom_js)) {
			?>
				<?php echo '<script>'. $custom_js. '</script> '; ?>
			<?php
		}
	?>
	</body>
</html>