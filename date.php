<?php
/**
 * The template for displaying news pages
 *
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<!-- tagline -->
<section class="h-tagline-title">
    <div class="cntr">
        <h4>アーカイブ</h4>
    </div>
</section>
<!-- tagline -->


<section class="h-page-news">
    <div class="cntr">
        <!-- breadcrumbs -->
        <ul class="h-breadcrumbs">
            <li>
                <a href="<?php bloginfo('home'); ?>">Top</a>
            </li>
            <li>
                <span>アーカイブ</span>
            </li>
        </ul>
        <!-- end of breadcrumbs -->
        <div class="gap gap-10 gap-0-xs">
            <div class="md-8 xs-12">
                <ul class="h-page-news-list">
                    <?php while (have_posts()) : the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>" class="h-page-news-list-item">
                            <div class="h-page-news-img">
                            <?php if(has_post_thumbnail()): ?>
                                <?php
                                    $thumb_id = get_post_thumbnail_id(get_the_ID());
                                    $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                                ?>
                                <img title="<?php the_title(); ?>" alt="<?php echo $alt; ?>" class="wp-post-image is-wide" src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
                            <?php else: ?>
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/thumb.jpg" alt="" class="is-wide">
                            <?php endif; ?>
                            </div>
                            <div class="h-page-news-cont">
                                <span class="h-page-news-date"><?php the_date('Y.m.d'); ?></span>
                                <?php
                                    $terms = get_the_terms( $post->ID , 'category' );
                                    if(is_array($terms) || is_object($terms)){
                                        foreach ( $terms as $term ) {
                                            ?>
                                            <span class="h-page-news-cat <?php echo $term->slug; ?>">
                                                <?php echo $term->name; ?>
                                            </span>
                                        <?php
                                        }
                                    }
                                ?>
                                <h4 class="h-page-news-title"><?php the_title(); ?></h4>
                            </div>
                        </a>
                    </li>
                    <?php endwhile;
                        wp_reset_query();
                    ?>
                </ul>
            </div>
            <div class="md-4 xs-12">
                <?php echo get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();